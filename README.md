# Docker


## Getting started

To start mysql database server locally, run the following command

```
docker-compose up
```

To test the connection, use any mysql client. The one that comes with IntelliJ IDEA is recommended. 
```